from django.apps import AppConfig


class ReferenceConfig(AppConfig):
    name = 'models.reference'
