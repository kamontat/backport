from rest_framework.test import APITestCase
from rest_framework import status


class WorkTest(APITestCase):
    def test_list_all_works(self):
        """
        Ensure we cannot list all the works
        """

        response = self.client.get('/v1/works/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
