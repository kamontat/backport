from rest_framework.test import APITestCase
from rest_framework import status


class Test(APITestCase):
    def test_example(self):
        """
        This is a example test api
        """

        response = self.client.get('/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
