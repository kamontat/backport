from rest_framework.test import APITestCase
from rest_framework import status


class LanguageTest(APITestCase):
    def test_list_all_languages(self):
        """
        Ensure we cannot list all language
        """

        response = self.client.get('/v1/languages/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
