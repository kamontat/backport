from api.serializers import BaseSerializer, WithCloudinary, WithOnlyKey
from models.tag.serializers import TagSerializer, TechnicalSerializer

from .models import Highlight, EPhoto


class EPhotoSerializer(BaseSerializer, WithCloudinary):
    cloudinary_fields = ['file']

    class Meta:
        model = EPhoto
        fields = ('name', 'file')


class HighLightSerializer(BaseSerializer, WithOnlyKey):
    only_field = 'name'

    class Meta:
        model = Highlight
        fields = ('name',)


class ExperienceSerializer(BaseSerializer, WithCloudinary):
    cloudinary_fields = ['banner']

    gallery = EPhotoSerializer(many=True, read_only=True)
    highlights = HighLightSerializer(many=True, read_only=True)
    tag = TagSerializer(many=True, read_only=True)
    technical = TechnicalSerializer(many=True, read_only=True)
