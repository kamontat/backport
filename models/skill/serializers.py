from api.serializers import BaseSerializer, WithTranslatable

from .models import Skill


class MiniSkillSerializer(BaseSerializer, WithTranslatable):
    def translate(self, return_object, instance):
        return_object['level'] = instance.translate_level(
            self.get_lang())
        return return_object

    class Meta:
        model = Skill
        fields = ('name', 'description', 'level')


class SkillSerializer(MiniSkillSerializer):
    class Meta:
        model = Skill
        fields = ('name', 'description', 'level', 'tag',
                  'page_order', 'created_at', 'updated_at')
