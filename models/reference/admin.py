from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithPageOrder, WithAutoOrderField
from .models import Reference


class ReferenceAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithPageOrder, WithPerson, WithAutoOrderField):
    pass


admin.site.register(Reference, ReferenceAdmin)
