from rest_framework.test import APITestCase
from rest_framework import status

# How to write the test
# ref: https://www.django-rest-framework.org/api-guide/testing/


class ChangeloglogTest(APITestCase):
    def test_query_changelog_page(self):
        """
        Ensure we can get changelog page
        """

        response = self.client.get('/changelog/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
