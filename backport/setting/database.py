# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

# Install PyMySQL as mysqlclient/MySQLdb to use Django's mysqlclient adapter
# See https://docs.djangoproject.com/en/2.1/ref/databases/#mysql-db-api-drivers
# for more information

import pymysql

from backport.setting.helper import get_env_variable

pymysql.install_as_MySQLdb()

CONNECTION_NAME = get_env_variable('CONNECTION_NAME')
DB_NAME = get_env_variable('DB_NAME')
DB_USER = get_env_variable('DB_USER')
DB_PASSWORD = get_env_variable('DB_PASSWORD')

# [START db_setup]
if get_env_variable('IS_GAE') != '':
    print("Starting... with Real database")
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': '/cloudsql/' + CONNECTION_NAME,
            'USER': DB_USER,
            'PASSWORD': DB_PASSWORD,
            'NAME': DB_NAME,
        }
    }
elif get_env_variable('CI') != '':
    TEST_DB_NAME = get_env_variable('TEST_DB_NAME')
    TEST_DB_USER = get_env_variable('TEST_DB_USER')
    TEST_DB_PASSWORD = get_env_variable('TEST_DB_PASSWORD')

    print("Starting... with Test database")
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': 'mysql',
            'PORT': '3306',
            'NAME': TEST_DB_NAME,
            'USER': TEST_DB_USER,
            'PASSWORD': TEST_DB_PASSWORD,
        }
    }
else:
    print("Starting... with Local database")
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': '127.0.0.1',
            'PORT': '3306',
            'NAME': DB_NAME,
            'USER': DB_USER,
            'PASSWORD': DB_PASSWORD,
        }
    }
