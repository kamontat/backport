# Generated by Django 2.0.8 on 2019-04-09 12:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personal', '0003_auto_20190319_1405'),
    ]

    operations = [
        migrations.AddField(
            model_name='personal',
            name='organization',
            field=models.TextField(blank=True, help_text='Current company or organization', max_length=200, null=True),
        ),
    ]
