from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithAutoOrderField

from .models import Personal, Social


class SocialInlineAdmin(admin.TabularInline):
    model = Social


class PersonalAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithAutoOrderField):
    fieldsets = (
        (None, {
            'fields': ('id', 'image', ('firstname', 'lastname'), 'nickname', ('email', 'phone_number'), 'birthday', 'myself', 'organization', ('created_at', 'updated_at'))
        }),
        ('Summary', {
            'fields': ('summary', ),
        }),
        ('Address', {
            'fields': (('address_name', 'address_name2'), ('address_city', 'address_country'), 'address_postal_code')
        }),
    )
    inlines = [SocialInlineAdmin, ]


class SocialAdmin(BaseAdmin, WithPerson, WithTranslatable, WithAutoOrderField):
    list_display = ('network', 'username')


admin.site.register(Personal, PersonalAdmin)
admin.site.register(Social, SocialAdmin)
