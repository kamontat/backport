from .models import Education
from api.serializers import BaseSerializer, WithTranslatable


class MiniEducationSerializer(BaseSerializer, WithTranslatable):
    def translate(self, return_object, instance):
        return_object['type'] = instance.translate_type(
            self.get_lang())

        if return_object.setdefault('grade_type', None) != None:
            return_object['grade_type'] = instance.translate_grade_type(
                self.get_lang())

        if return_object.setdefault('pass_or_fail', None) != None:
            return_object['pass_or_fail'] = instance.translate_pass_or_fail(
                self.get_lang())

        return return_object

    class Meta:
        model = Education
        fields = ('institution', 'area', 'type')


class EducationSerializer(MiniEducationSerializer):
    class Meta:
        model = Education
        fields = ('institution', 'area', 'type', 'grade_type', 'score', 'pass_or_fail',
                  'start_date', 'end_date', 'certificate', 'page_order', 'created_at', 'updated_at')
