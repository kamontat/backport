"""backport URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from jet.dashboard.dashboard_modules import google_analytics_views

from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

from rest_framework.documentation import include_docs_urls

from version.views import VersionView
from changelog.views import ChangelogView
from index.views import indexPage


urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    # path('accounts/', include('django.contrib.auth.urls')), # api docs should availble only auth user

    path('docs/', include_docs_urls(title='Personal APIs', public=False)),
    path('v1/', include('api.urls')),
    path('version/', VersionView.as_view()),
]

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('admin/docs/', include('django.contrib.admindocs.urls')),
    prefix_default_language=False
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += [
    path('', indexPage),
    path('changelog/', ChangelogView.as_view()),
]
