from auditlog.registry import auditlog

from django.db import models
from translations.models import Translatable

from ckeditor.fields import RichTextField
from cloudinary.models import CloudinaryField


class Personal(Translatable):
    """
    Personal information
    """
    id = models.SlugField(max_length=10, primary_key=True,
                          help_text="ID that represent information")
    image = CloudinaryField('image', help_text="Picture of person")
    firstname = models.CharField(max_length=100, help_text="person first name")
    lastname = models.CharField(max_length=100, help_text="person last name")
    nickname = models.CharField(
        max_length=20, blank=True, null=True, help_text="person nickname")
    email = models.EmailField(help_text="person email")
    birthday = models.DateField(
        blank=True, null=True, help_text="personal birthday in format yyyy-mm-dd")
    address_name = models.CharField(
        max_length=100, blank=True, null=True, help_text="this should be house number and street")
    address_name2 = models.CharField(
        max_length=100, blank=True, null=True, help_text="addition address information")
    address_city = models.CharField(
        max_length=30, blank=True, null=True, help_text="city in your country")
    address_country = models.CharField(
        max_length=30, blank=True, null=True, help_text="address country")
    address_postal_code = models.CharField(
        max_length=8, blank=True, null=True, help_text="postal code or post code")
    phone_number = models.CharField(
        max_length=20, blank=True, null=True, help_text="mobile phone number")
    myself = models.TextField(max_length=200, blank=True, null=True,
                              help_text="noun that decribe yourself, this should be short")
    organization = models.CharField(max_length=100, blank=True, null=True,
                                    help_text="Current company or organization")
    summary = RichTextField(blank=True, null=True,
                            help_text="summary your profile")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class TranslatableMeta:
        fields = ['firstname', 'lastname', 'nickname', 'address_name',
                  'address_name2', 'address_city', 'address_country', 'myself', 'summary']

    def __str__(self):
        return "{} {}".format(self.firstname, self.lastname)


class Social(Translatable):
    """
    A social information and contact to specify personal information
    """

    network = models.CharField(max_length=50, help_text="social media name")
    username = models.CharField(
        max_length=50, help_text="username of current social media")
    url = models.URLField(
        help_text="this should be direct link to profile social media")

    who = models.ForeignKey(
        Personal,
        on_delete=models.CASCADE,
        help_text="this is a owner of social media profile"
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class TranslatableMeta:
        fields = ['network']

    def __str__(self):
        return "username of {} is {}".format(self.network, self.username)


auditlog.register(Personal)
auditlog.register(Social)
