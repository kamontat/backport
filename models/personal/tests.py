from rest_framework.test import APITestCase
from rest_framework import status


class PersonalTest(APITestCase):
    def test_get_personal(self):
        """
        Ensure we cannot list all personal information in website
        """

        response = self.client.get('/v1/information/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_list_all_socials(self):
        """
        Ensure we cannot list all social media
        """

        response = self.client.get('/v1/socials/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
