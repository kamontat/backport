
from api.serializers import BaseSerializer, WithTranslatable

from .models import Language


class MiniLanguageSerializer(BaseSerializer, WithTranslatable):
    def translate(self, return_object, instance):
        return_object['proficiency'] = instance.translate_proficiency(
            self.get_lang())
        return return_object

    class Meta:
        model = Language
        fields = ('name', 'proficiency')


class LanguageSerializer(MiniLanguageSerializer):

    class Meta:
        model = Language
        fields = ('name', 'proficiency', 'created_at', 'updated_at')
