from api import views

from api.filters import WithOwner, WithPageOrder

from .serializers import Project, ProjectSerializer

from models.experience.views import ExperienceViewSet


class ProjectFilter(WithOwner, WithPageOrder):
    """
    Filter project in several format and columns
    """

    class Meta:
        model = Project
        fields = ['who', 'technical', 'tag', 'name']  # add name is filterable


class ProjectViewSet(ExperienceViewSet):
    """
    API endpoint that allows users able to get projects list for specify personal information
    """

    queryset = Project.objects.filter(
        page_order__gte=0).order_by('-start_date', '-end_date')
    serializer_class = ProjectSerializer
    filter_class = ProjectFilter
