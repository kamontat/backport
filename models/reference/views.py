from api import views

from api.filters import WithOwner, WithPageOrder
from .serializers import Reference, ReferenceSerializer


class ReferenceFilter(WithOwner, WithPageOrder):
    class Meta:
        model = Reference
        fields = ['who']


class ReferenceViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of interests in specify personal information
    """

    required_queries = ['who']
    queryset = Reference.objects.order_by('id')
    serializer_class = ReferenceSerializer
    filter_class = ReferenceFilter
