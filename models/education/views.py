from api import views

from .models import Education
from .serializers import EducationSerializer

import rest_framework_filters as filters
from api.filters import WithOwner, WithPageOrder


class EducationFilter(WithOwner, WithPageOrder):
    type = filters.CharFilter(
        help_text="A type of education")

    class Meta:
        model = Education
        fields = ['who', 'type']


class EducationViewSet(views.IncludeListAPI):
    """
    API endpoint that allows users able to view education list
    """

    required_queries = ['who']
    queryset = Education.objects.filter(
        page_order__gte=0).order_by('-start_date', '-end_date')
    serializer_class = EducationSerializer
    filter_class = EducationFilter
