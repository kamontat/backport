from auditlog.registry import auditlog

from django.db import models
from api.models import BaseModel, WithPerson, WithPageOrder, WithTranslatable, WithStartEndable, WithCloudinary

from model_utils.managers import InheritanceManager
from models.tag.models import Tag, Technical

from ckeditor.fields import RichTextField
from cloudinary.models import CloudinaryField

import uuid


class EPhoto(BaseModel, WithCloudinary):
    cloudinary_name = "file"

    name = models.CharField(
        max_length=50, unique=True, default=uuid.uuid4)
    file = CloudinaryField(
        'image', help_text="image in experience category")

    @property
    def file_url(self):
        return self.file.url

    class Meta:
        verbose_name_plural = "gallery"

    def __str__(self):
        return self.name


class Experience(BaseModel, WithPerson, WithPageOrder, WithStartEndable, WithCloudinary, WithTranslatable):
    cloudinary_name = "banner"

    banner = CloudinaryField(
        'image', help_text="banner of the frontend", blank=True, null=True)

    summary = models.TextField(
        max_length=200, help_text="The short script to summary your work in 200 words")

    body = RichTextField(
        blank=True, null=True, help_text="Deep information of your work")

    tag = models.ManyToManyField(
        Tag, blank=True)

    technical = models.ManyToManyField(
        Technical, blank=True)

    gallery = models.ManyToManyField(
        EPhoto, blank=True, related_name='experience')

    def __str__(self):
        instance = Experience.inheritance.get_subclass(id=self.id)
        if type(instance) is not Experience:
            return instance.__str__()

        return 'Experience: {}'.format(self.id)

    def get_translatable_meta():
        return ('summary', 'body')


class Highlight(BaseModel, WithTranslatable):
    name = models.CharField(
        max_length=100, help_text="Short description for the keyword of experience")

    experience = models.ForeignKey(
        Experience, null=True, on_delete=models.SET_NULL, related_name="highlights")

    class TranslatableMeta:
        fields = ('name',)

    def __str__(self):
        return "{}".format(self.name)


auditlog.register(Highlight)
auditlog.register(EPhoto)
