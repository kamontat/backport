from rest_framework.test import APITestCase
from rest_framework import status


class ReferenceTest(APITestCase):
    def test_list_all_skill(self):
        """
        Ensure we cannot list all available references
        """

        response = self.client.get('/v1/references/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
