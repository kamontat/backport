from collections import OrderedDict

from django.contrib import admin
from django.contrib import messages
from translations.admin import TranslatableAdminMixin, TranslationInline


class BaseAdmin(admin.ModelAdmin):
    pass


class WithTranslatable(TranslatableAdminMixin, admin.ModelAdmin):
    def get_inline_instances(self, request, obj=None):
        self.inlines = set(self.inlines)
        self.inlines.add(TranslationInline)

        inlines = super().get_inline_instances(request, obj)
        self.prepare_translation_inlines(inlines, TranslationInline)
        return inlines


class SortableField():
    def get_order_keys(self):
        return set()


class ExtendAdminView(admin.ModelAdmin, SortableField):
    """
    Add id created_at and updated_at to admin page
    """

    readonly_fields = ('id', 'created_at', 'updated_at')

    def get_order_keys(self):
        keys = super().get_order_keys()
        keys.add('extend')
        return keys


class WithPerson(admin.ModelAdmin, SortableField):
    def get_list_display(self, request):
        return super().get_list_display(request) + ('who',)

    def get_list_filter(self, request):
        return super().get_list_filter(request) + ('who',)

    def get_order_keys(self):
        keys = super().get_order_keys()
        keys.add('who')
        return keys


class WithPageOrder(admin.ModelAdmin, SortableField):
    actions = ['reset_page_order']

    def get_ordering(self, request):
        return super().get_ordering(request) + ('page_order',)

    def get_list_display(self, request):
        self.list_display_links = (super().get_list_display(request)[0],)
        return ('page_order',) + super().get_list_display(request)

    def get_list_filter(self, request):
        return super().get_list_filter(request) + ('page_order',)

    def get_order_keys(self):
        keys = super().get_order_keys()
        keys.add('page_order')
        return keys

    def reset_page_order(self, request, queryset):
        affected = queryset.update(page_order=0)
        self.message_user(
            request, "{} row had been reseted page order to zero.".format(affected), level=messages.SUCCESS)

    reset_page_order.short_description = "Reset page order to zero"


class WithAutoOrderField(admin.ModelAdmin, SortableField):
    def get_fields(self, request, obj=None):
        custom_fields = []
        keys = self.get_order_keys()

        if 'extend' in keys and obj:
            custom_fields.append('id')

        if 'who' in keys:
            custom_fields.append('who')

        if self.fields:
            custom_fields.extend(self.fields)
        else:
            get_fields = super().get_fields(request, obj)

            if 'extend' in keys and obj:
                if 'id' in get_fields:
                    get_fields.remove('id')
            if 'who' in keys:
                if 'who' in get_fields:
                    get_fields.remove('who')
            custom_fields.extend(get_fields)

        if 'page_order' in keys:
            if 'page_order' in custom_fields:
                custom_fields.remove('page_order')
            custom_fields.append('page_order')

        if 'extend' in keys and obj:
            if 'created_at' in custom_fields:
                custom_fields.remove('created_at')
            custom_fields.append('created_at')
            if 'updated_at' in custom_fields:
                custom_fields.remove('updated_at')
            custom_fields.append('updated_at')

        return custom_fields
