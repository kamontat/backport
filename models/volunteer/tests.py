from rest_framework.test import APITestCase
from rest_framework import status


class VolunteerTest(APITestCase):
    def test_list_all_volunteer(self):
        """
        Ensure we cannot list all volunteer
        """

        response = self.client.get('/v1/volunteers/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
