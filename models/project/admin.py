from models.experience.abstract.admin import ExperienceAdmin
from .models import Project


class ProjectAdmin(ExperienceAdmin):
    experience_title_fields = ('name', 'link')
    list_display = ('name',)
