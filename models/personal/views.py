from api import views

from .models import Personal, Social
from .serializers import PersonalSerializer, SocialSerializer

import rest_framework_filters as filters
from api.filters import WithOwner


class PersonalViewSet(views.IncludeRetrieveAPI):
    """
    API endpoint that allows users to be viewed personal information.
    """
    queryset = Personal.objects.all()
    serializer_class = PersonalSerializer


class SocialFilter(WithOwner):
    network = filters.CharFilter(
        help_text="Network name of social media ")

    class Meta:
        model = Social
        fields = ['who', 'network']
        help_text = 'social filter'


class SocialViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of user social contact
    """

    required_queries = ['who']
    queryset = Social.objects.order_by('id')
    serializer_class = SocialSerializer
    filter_class = SocialFilter
