from corsheaders.defaults import default_methods, default_headers

CORS_ALLOW_METHODS = default_methods + (
    'HEAD',
)

CORS_ALLOW_HEADERS = default_headers + (
    'KC-Request',
    'Access-Control-Allow-Origin'
)

CORS_ORIGIN_ALLOW_ALL = True
