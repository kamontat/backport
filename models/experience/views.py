from api import views

# Create your views here.


class ExperienceViewSet(views.IncludeListAPI, views.IncludeRetrieveAPI):
    required_queries = ['who']
    translate_fields = ['tag', 'technical']
