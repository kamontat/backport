CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor/"

CKEDITOR_CONFIGS = {
    'default': {
        "skin": 'office2013',
        'toolbar_custom': [
            {'name': 'tools', 'items': ['Maximize']},
            {'name': 'document', 'items': [
                'Source', '-', 'Save', 'Preview', 'Print']},
            {'name': 'editing', 'items': [
                'Find', '-', 'SelectAll']},
            {'name': 'about',
             'items': ['About']},
            "/",
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat']},
            {'name': 'styles', 'items': [
                'Format', 'TextColor']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'CreateDiv', 'Blockquote', '-',
                       'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
            {'name': 'links', 'items': ['Image', 'Link', 'Unlink']},
            {'name': 'insert',
             'items': ['HorizontalRule', 'SpecialChar']},
        ],
        'toolbar': 'custom',
        'toolbarCanCollapse': True,
        'tabSpaces': 4,
        'extraPlugins': ','.join([
            'uploadimage',  # the upload image feature
            # your extra plugins here
            'div',
            'autolink',
            'autoembed',
            'embedsemantic',
            'autogrow',
            'devtools',
            'widget',
            'lineutils',
            'dialog',
            'dialogui',
            'elementspath'
        ]),
    },
    "mini": {
        "skin": 'minimalist',
        'toolbar_custom': [
            {'name': 'document', 'items': ['Source']},
            {'name': 'basicstyles',
             'items': ['Format', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor', '-', 'RemoveFormat']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'CreateDiv', 'Blockquote']},
            "/",
            {'name': 'links', 'items': ['Image', 'Link', 'Unlink']},
        ],
        'toolbar': 'custom',
        'toolbarCanCollapse': True,
        'tabSpaces': 4,
        'extraPlugins': ','.join([
            'uploadimage',  # the upload image feature
            # your extra plugins here
            'div',
            'autolink',
            'autoembed',
            'embedsemantic',
            'autogrow',
            'devtools',
            'widget',
            'lineutils',
            'dialog',
            'dialogui',
            'elementspath'
        ]),
    }
}
