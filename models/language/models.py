from auditlog.registry import auditlog

from django.db import models
from api.models import BaseModel, WithTranslatable, WithPerson


class Language(BaseModel, WithTranslatable, WithPerson):
    # TODO: enable translate to proficiency choices
    PROFICIENCY_CHOICES = (
        ('elementary', 'Elementary proficiency'),
        ('limit', 'Limited working proficiency'),
        ('professional', 'Professional working proficiency'),
        ('full', 'Full professional proficiency'),
        ('native', 'Native or bilingual proficiency'),
    )

    # Conversion to a dictionary mapping
    PROFICIENCY_CHOICES_DICT = dict(PROFICIENCY_CHOICES)

    PROFICIENCY_CHOICES_TH = (
        ('elementary', 'ความสามารถขั้นพื้นฐาน'),
        ('limit', 'ความสามารถในการทำงานขั้นพื้นฐาน'),
        ('professional', 'ความสามารถในการทำงานอย่างมืออาชีพ'),
        ('full', 'ความสามารถในการทำงานอย่างมืออาชีพเต็มรู้แบบ'),
        ('native', 'ความสามารถทางภาษาพื้นเมืองหรือสองภาษา'),
    )

    # Conversion to a dictionary mapping
    PROFICIENCY_CHOICES_TH_DICT = dict(PROFICIENCY_CHOICES_TH)

    name = models.CharField(
        max_length=100, help_text="language name")

    proficiency = models.CharField(
        max_length=20, choices=PROFICIENCY_CHOICES,
        help_text="The ability of an individual to speak or perform in a language")

    class TranslatableMeta:
        fields = ('name',)

    def translate_proficiency(self, lang):
        if lang == 'th':
            return self.PROFICIENCY_CHOICES_TH_DICT[self.proficiency]
        else:
            return self.PROFICIENCY_CHOICES_DICT[self.proficiency]

    def __str__(self):
        return "{} ({})".format(self.name, self.proficiency)


auditlog.register(Language)
