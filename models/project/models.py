from auditlog.registry import auditlog

from django.db import models
from models.experience.models import Experience

from cloudinary.models import CloudinaryField


class Project(Experience):
    name = models.CharField(
        max_length=100, help_text="name of the project")
    link = models.URLField(
        blank=True, null=True,
        help_text="A url link to project website")

    class TranslatableMeta:
        fields = Experience.get_translatable_meta() + ('name',)

    class Meta:
        app_label = 'experience'

    def __str__(self):
        return "{}".format(self.name)


auditlog.register(Project)
