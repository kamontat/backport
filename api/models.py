from django.db import models
from django.utils.html import format_html

from translations.models import Translatable, TranslatableQuerySet

from models.personal.models import Personal
from models.tag.models import Tag

from model_utils.managers import InheritanceManager


class BaseModel(models.Model):
    inheritance = InheritanceManager()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class WithPageOrder(models.Model):
    page_order = models.SmallIntegerField(
        default=0, blank=True, null=True,
        help_text="A order of the response JSON (negative to excluded)")

    class Meta:
        abstract = True


class WithTranslatable(Translatable):
    objects = TranslatableQuerySet.as_manager()

    class Meta:
        abstract = True


class WithPerson(models.Model):
    who = models.ForeignKey(
        Personal,
        on_delete=models.CASCADE,
    )

    class Meta:
        abstract = True


class WithStartEndable(models.Model):
    start_date = models.DateField(
        blank=True, null=True, help_text="yyyy-mm-dd")

    end_date = models.DateField(blank=True, null=True, help_text="yyyy-mm-dd")

    class Meta:
        abstract = True


class WithTagable(models.Model):
    tag = models.ManyToManyField(
        Tag, blank=True)

    class Meta:
        abstract = True


class WithCloudinary(models.Model):
    cloudinary_name = None

    def a_link(self):
        url = getattr(self, self.cloudinary_name).url
        return format_html('<a href="{}" target="_blank">{}</a>',
                           url, url)

    a_link.allow_tags = True

    class Meta:
        abstract = True
