from api import views

from api.filters import WithOwner, WithPageOrder

from .serializers import Work, WorkSerializer

from models.experience.views import ExperienceViewSet


class WorkFilter(WithOwner, WithPageOrder):
    """
    Filter work in several format and columns
    """

    class Meta:
        model = Work
        fields = ['who', 'technical', 'tag']


class WorkViewSet(ExperienceViewSet):
    """
    API endpoint that allows users able to get works list for specify personal information
    """

    queryset = Work.objects.filter(
        page_order__gte=0).order_by('-start_date', '-end_date')
    serializer_class = WorkSerializer
    filter_class = WorkFilter
