<a name="unreleased"></a>
## [Unreleased]


<a name="v1.2.2"></a>
## [v1.2.2] - 2019-04-09
### Improving application
- **admin:** change organization text field to char field


<a name="v1.2.1"></a>
## [v1.2.1] - 2019-04-09
### Fixes Bug
- **query:** 500 error when no who occurred


<a name="v1.2.0"></a>
## [v1.2.0] - 2019-04-09
### Fixes Bug
- **education:** cannot open education admin page 500 error
- **error:** try to wide error in python

### Improving application
- **personal:** add organization to personal information


<a name="v1.1.0"></a>
## [v1.1.0] - 2019-03-24
### Improving application
- **cloudinary:** delete image along with model


<a name="v1.0.0"></a>
## [v1.0.0] - 2019-03-19
### Feature
- **apis:** add receive individual experience tasks

### Fixes Bug
- **code:** error code not exist

### Improving application
- **api:** add id to list and receive response


<a name="v0.9.3"></a>
## [v0.9.3] - 2019-03-19
### Documentation
- **changelog:** apply markdown format to linter

### Improving application
- **admin:** add banner image to admin page
- **api:** add banner image to experiences apis
- **apis:** add id to response json
- **model:** add banner to all experiences


<a name="v0.9.2"></a>
## [v0.9.2] - 2019-03-18
### Improving application
- **ui:** change experince ui and fix volunteer error


<a name="v0.9.1"></a>
## [v0.9.1] - 2019-03-15
### Improving application
- **https:** force cloudinary to be https


<a name="v0.9.0"></a>
## [v0.9.0] - 2019-03-14
### Improving application
- **social:** make social network translatable


<a name="v0.8.8"></a>
## [v0.8.8] - 2019-03-14
### Improving application
- **cors:** add access-control-allow-origin to allow list


<a name="v0.8.7"></a>
## [v0.8.7] - 2019-03-14
### Improving application
- **cors:** add allow all to cors to avoid error on development


<a name="v0.8.6"></a>
## [v0.8.6] - 2019-03-14

<a name="v0.8.5"></a>
## [v0.8.5] - 2019-03-14
### Fixes Bug
- **code:** forget to make str to tuple


<a name="v0.8.4"></a>
## [v0.8.4] - 2019-03-14
### Improving application
- **api:** include summary to mini serializer
- **security:** remove allow all but only same domain


<a name="v0.8.3"></a>
## [v0.8.3] - 2019-03-13
### Improving application
- **cors:** add support allow all cors request


<a name="v0.8.2"></a>
## [v0.8.2] - 2019-03-13
### Fixes Bug
- **personal:** add missing image in personal api


<a name="v0.8.1"></a>
## [v0.8.1] - 2019-03-13
### Improving application
- **translate:** on summary api, you can get with any language


<a name="v0.8.0"></a>
## [v0.8.0] - 2019-03-12
### Documentation
- **serializer:** add to translate in serializer that different

### Feature
- **api:** improve summary api and add ?tag=<tag|technical>
- **translate:** make level to be translatable and add full text

### Fixes Bug
- **typo:** error code on pagination

### Improving application
- **api:** add translatable to education constant
- **api:** now you can filter using name in project

### Improving performance
- **serializer:** add mini serial to ref & volu to improve speed


<a name="v0.7.0"></a>
## [v0.7.0] - 2019-03-11
### Feature
- **api:** add new api call summary '/make/summary'

### Improving application
- **api:** improve error handler in rest apis
- **exception:** improve base exception format

### Improving performance
- **serializer:** add minimal serializer for quick query


<a name="v0.6.0"></a>
## [v0.6.0] - 2019-03-10
### Feature
- **api:** add /list/all to receive both tag and technical

### Improving application
- **view:** add multiple models returnable in one request


<a name="v0.5.6"></a>
## [v0.5.6] - 2019-03-06
### Feature
- **admin:** add reset multiple page order

### Fixes Bug
- **admin:** order of columns in admin page
- **admin:** page order position in skill page

### Improving application
- **api:** add cludinary to project banner
- **api:** update inheritance manager to other attribute name


<a name="v0.5.5"></a>
## [v0.5.5] - 2019-03-06
### Feature
- **format:** support multiple response format: json, yaml, xml

### Improving application
- **admin:** add list display and filter in language admin
- **cloudinary:** update cloudinary link in admin page
- **serializer:** improve serializer with With keyword
- **translate:** implement translatable serializer


<a name="v0.5.4"></a>
## [v0.5.4] - 2019-03-04
### Improving application
- **http:** force redirect to https


<a name="v0.5.3"></a>
## [v0.5.3] - 2019-03-04
### Improving application
- **admin:** add who filter in several admin page
- **admin:** integrate new admin class with education admin
- **admin:** create new model abstract to work with new admin
- **admin:** update relationship between photo and experiences
- **ci:** build on tags only and test all except tag
- **helper:** add script helper
- **model:** add name and adopt to new abstract models
- **model:** create new abstract model inside application


<a name="v0.5.2"></a>
## [v0.5.2] - 2019-03-03
### Improving application
- **api:** improve version endpoint, add date
- **changelog:** update changelog title color
- **index:** add more changelog link to index page
- **version:** improve version algorithm in deployment


<a name="v0.5.1"></a>
## [v0.5.1] - 2019-03-03
### Feature
- **model:** add reference to apis and admin page
- **model:** update all basic model, admin and apis
- **model:** add new model call interest

### Fixes Bug
- **api:** make gallery return as gallery json string
- **api:** reference not exist in apis
- **model:** forget to add in install app variable
- **typo:** update typo of page_order
- **typo:** update descript column in admin page


<a name="v0.4.0"></a>
## [v0.4.0] - 2019-03-02
### Feature
- **model:** add new models and api of skill

### Improving application
- **dashboard:** separate audit log to different collapse modules


<a name="v0.3.0"></a>
## [v0.3.0] - 2019-03-02
### Feature
- **api:** add 2 new more apis volunteer and language
- **model:** add project model to admin and apis
- **page:** add changelog page
- **translate:** add translatable in admin page
- **translate:** make translate to relation models as well

### Fixes Bug
- **admin:** update model to experiences instead
- **lib:** green not working
- **test:** error path in testcase

### Improving application
- **apis:** make models translatable to relation models
- **experience:** improve all models that connect to experiences


<a name="v0.2.0"></a>
## [v0.2.0] - 2019-03-02
### Feature
- **admin:** add tab inline support to highlight
- **api:** add /list/tags and /list/tech...
- **api:** add works json support at /v1/works/<name>

### Fixes Bug
- who in admin page not exist
- **admin:** error when update new model via popup
- **admin:** cannot create new model via popup creator
- **model:** make page order is negative number support

### Improving application
- **apis:** exclude page order that low than 0
- **ci:** add ci integration for testing
- **log:** add auditlog support to work
- **model:** add relation between experience and personal


<a name="0.1.0"></a>
## [0.1.0] - 2019-03-01
### Documentation
- **changelog:** update

### Feature
- **api:** introduce new model call 'tag'
- **model:** add work to admin and database
- **model:** add root experience models

### Improving application
- **admin:** integrate photo gallery in admin page
- **model:** improve tag and technical keywords
- **model:** change personal myself to multiple line text


<a name="v0.0.1"></a>
## v0.0.1 - 2019-02-27
### Feature
- **admin:** add auditlog
- **cms:** add new table call personal information
- **core:** implement custom serializer and views
- **css:** improve a lot of admin ui
- **db:** improve database to mysql instead
- **docs:** add docs page in admin page
- **education:** add education apis and admin page
- **filter:** include filter query parameter in url
- **filter:** include filter query to education view and apis
- **i18n:** add language changable in admin page
- **i18n:** enable create i18n model
- **image:** include cloudiary to image instead of local
- **page:** add index which return all apis availble
- **sql:** add database to git
- **translate:** change whole translate model apis

### Fixes Bug
- **api:** translate not exist in social media
- **build:** build error in gitlab ci
- **ci:** try fourth time
- **ci:** try second time
- **ci:** deployment error by reader is not correct
- **ci:** forget to include db config in .env
- **ci:** try third time
- **ci:** final fix
- **ci:** tenth time
- **ci:** ninth time
- **ci:** eighth time
- **ci:** sixth time
- **ci:** try fifth time
- **ci :** seventh time
- **css:** editor css not found
- **database:** must use real db instead of localhost
- **db:** wrong connection name
- **db:** deployed db error
- **db:** make migrate should called by developer
- **debug:** dynamic debug mode
- **error:** define missing method
- **ga:** include ga key to deployment
- **lib:** lib error
- **lib:** missing lib version
- **lib:** include build js bundle
- **logger:** resolve internal error cause by log invalid
- **model:** abstract model error in education
- **try:** auto find .env instead
- **try:** env file not found on gcp
- **try:** avoid env not found
- **try:** fix env not found in production
- **ui:** selection is always error when choose
- **url:** include education url

### Improving application
- **abstract:** add abstract and mixin class to help code quality
- **admin:** add social tab in admin page
- **api:** add version api and fix build error
- **api:** update version APIs
- **ci:** stop previous version to avoid exceed cost
- **ckeditor:** add static file of ckeditor lib
- **config:** allow all host domain
- **css:** collect any css in django
- **filter:** add filter default to url path as a query
- **lib:** change external lib to internal to custom error
- **log:** enable logging in production
- **model:** update personal model
- **security:** change token key and debug mode
- **util:** add utils to support view and documents


[Unreleased]: https://gitlab.com/kamontat/backport/compare/v1.2.2...HEAD
[v1.2.2]: https://gitlab.com/kamontat/backport/compare/v1.2.1...v1.2.2
[v1.2.1]: https://gitlab.com/kamontat/backport/compare/v1.2.0...v1.2.1
[v1.2.0]: https://gitlab.com/kamontat/backport/compare/v1.1.0...v1.2.0
[v1.1.0]: https://gitlab.com/kamontat/backport/compare/v1.0.0...v1.1.0
[v1.0.0]: https://gitlab.com/kamontat/backport/compare/v0.9.3...v1.0.0
[v0.9.3]: https://gitlab.com/kamontat/backport/compare/v0.9.2...v0.9.3
[v0.9.2]: https://gitlab.com/kamontat/backport/compare/v0.9.1...v0.9.2
[v0.9.1]: https://gitlab.com/kamontat/backport/compare/v0.9.0...v0.9.1
[v0.9.0]: https://gitlab.com/kamontat/backport/compare/v0.8.8...v0.9.0
[v0.8.8]: https://gitlab.com/kamontat/backport/compare/v0.8.7...v0.8.8
[v0.8.7]: https://gitlab.com/kamontat/backport/compare/v0.8.6...v0.8.7
[v0.8.6]: https://gitlab.com/kamontat/backport/compare/v0.8.5...v0.8.6
[v0.8.5]: https://gitlab.com/kamontat/backport/compare/v0.8.4...v0.8.5
[v0.8.4]: https://gitlab.com/kamontat/backport/compare/v0.8.3...v0.8.4
[v0.8.3]: https://gitlab.com/kamontat/backport/compare/v0.8.2...v0.8.3
[v0.8.2]: https://gitlab.com/kamontat/backport/compare/v0.8.1...v0.8.2
[v0.8.1]: https://gitlab.com/kamontat/backport/compare/v0.8.0...v0.8.1
[v0.8.0]: https://gitlab.com/kamontat/backport/compare/v0.7.0...v0.8.0
[v0.7.0]: https://gitlab.com/kamontat/backport/compare/v0.6.0...v0.7.0
[v0.6.0]: https://gitlab.com/kamontat/backport/compare/v0.5.6...v0.6.0
[v0.5.6]: https://gitlab.com/kamontat/backport/compare/v0.5.5...v0.5.6
[v0.5.5]: https://gitlab.com/kamontat/backport/compare/v0.5.4...v0.5.5
[v0.5.4]: https://gitlab.com/kamontat/backport/compare/v0.5.3...v0.5.4
[v0.5.3]: https://gitlab.com/kamontat/backport/compare/v0.5.2...v0.5.3
[v0.5.2]: https://gitlab.com/kamontat/backport/compare/v0.5.1...v0.5.2
[v0.5.1]: https://gitlab.com/kamontat/backport/compare/v0.4.0...v0.5.1
[v0.4.0]: https://gitlab.com/kamontat/backport/compare/v0.3.0...v0.4.0
[v0.3.0]: https://gitlab.com/kamontat/backport/compare/v0.2.0...v0.3.0
[v0.2.0]: https://gitlab.com/kamontat/backport/compare/0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/kamontat/backport/compare/v0.0.1...0.1.0
