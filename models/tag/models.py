from auditlog.registry import auditlog

from django.db import models
from translations.models import Translatable


class Technical(Translatable):
    """
    This is a technical name and stack for example programming language, frameworks.
    """

    name = models.CharField(max_length=50, unique=True,
                            help_text="This is a name of the technical")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class TranslatableMeta:
        fields = ['name']

    def __str__(self):
        return "{}".format(self.name)


class Tag(Translatable):
    """
    This should be a tag to work and project
    """

    name = models.CharField(max_length=50, unique=True,
                            help_text="This is a name of tag")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class TranslatableMeta:
        fields = ['name']

    def __str__(self):
        return "{}".format(self.name)


auditlog.register(Technical)
auditlog.register(Tag)
