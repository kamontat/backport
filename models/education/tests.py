from rest_framework.test import APITestCase
from rest_framework import status


class EducationTest(APITestCase):
    def test_list_all_educations(self):
        """
        Ensure we cannot list all educations
        """

        response = self.client.get('/v1/educations/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
