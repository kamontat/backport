from rest_framework.test import APITestCase
from rest_framework import status

# How to write the test
# ref: https://www.django-rest-framework.org/api-guide/testing/


class VersionTest(APITestCase):
    def test_get_current_version(self):
        """
        Ensure we can get current version
        """

        response = self.client.get('/version/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
