from api.serializers import BaseSerializer, WithOnlyKey

from .models import Tag, Technical


class TagSerializer(BaseSerializer, WithOnlyKey):
    only_field = 'name'

    class Meta:
        model = Tag
        fields = ('name',)


class TechnicalSerializer(BaseSerializer, WithOnlyKey):
    only_field = 'name'

    class Meta:
        model = Technical
        fields = ('name',)
