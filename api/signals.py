from django.db.models.signals import pre_delete
from django.dispatch import receiver

import cloudinary

@receiver(pre_delete)
def photo_delete(sender, instance, **kwargs):
    try:
      resource = getattr(instance, instance.cloudinary_name)
      print('Deleted {} cloudinary image', resource.url)
      cloudinary.uploader.destroy(resource.public_id)
    except AttributeError:
      print('DONT deleted any cloudinary images')
    except Exception as inst:
      print("SOME ERROR OCCURRED")
      print(inst)
