from models.experience.serializers import ExperienceSerializer

from .models import Project


class ProjectSerializer(ExperienceSerializer):
    class Meta:
        model = Project
        fields = ('id', 'banner', 'name', 'link',
                  'start_date', 'end_date', 'tag', 'technical', 'summary', 'body',
                  'gallery', 'highlights', 'page_order', 'created_at', 'updated_at')


class MiniProjectSerializer(ExperienceSerializer):
    class Meta:
        model = Project
        fields = ('id', 'banner', 'name', 'summary')
