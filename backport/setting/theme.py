import os

from backport.setting.variable import BASE_DIR

JET_DEFAULT_THEME = 'light-gray'
JET_SIDE_MENU_COMPACT = True

JET_INDEX_DASHBOARD = 'backport.dashboard.MyDashboard'

JET_MODULE_GOOGLE_ANALYTICS_CLIENT_SECRETS_FILE = os.path.join(BASE_DIR, 'client_secrets.json')