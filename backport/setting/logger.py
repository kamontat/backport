from backport.setting.variable import DEBUG

level = 'DEBUG'
if DEBUG:
    level = 'INFO'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': level,
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': level,
        },
        'backport': {
            'handlers': ['console'],
            'level': level,
        },
        'jet': {
            'handlers': ['console'],
            'level': level,
        },
    }
}
