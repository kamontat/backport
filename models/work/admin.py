
from models.experience.abstract.admin import ExperienceAdmin
from .models import Work


class WorkAdmin(ExperienceAdmin):
    experience_title_fields = (('company', 'position'), 'website')
    list_display = ('position', 'company')
