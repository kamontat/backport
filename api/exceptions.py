from django.http import Http404

from rest_framework import status as http_status
from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException


def handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['error'] = True
        response.data['status_code'] = response.status_code
        response.data['message'] = response.data['detail']
        if isinstance(exc, Http404):
            response.data['code'] = 'user_error'
        else:
            print(exc.get_codes())
            response.data['code'] = exc.get_codes()
        response.data.pop("detail", None)

    return response


class InvalidQuery(APIException):
    status_code = http_status.HTTP_400_BAD_REQUEST
    default_code = 'query_invalid'
