from auditlog.registry import auditlog

from api.models import BaseModel, WithPerson, WithPageOrder, WithTranslatable, WithStartEndable
from django.db import models

from cloudinary.models import CloudinaryField


class Education(BaseModel, WithPageOrder, WithPerson, WithStartEndable, WithTranslatable):
    """
    Education information, this will collect all offline courses and online courses
    """

    CHOICES = [
        ('junior', 'Junior High School'),
        ('senior', 'Senior High School'),
        ('high-vocational', 'High Vocational Certificate'),
        ('technical-certificate', 'Technical Certificate'),
        ('non-formal', 'Non-Formal Education'),
        ('online', 'Online courses'),
        ('bachelor', 'Bachelor\'s degree'),
        ('master', 'Master\'s degree'),
        ('phd', 'Doctor of Philosophy (Ph.D.)'),
    ]
    CHOICES_DICT = dict(CHOICES)

    CHOICES_TH = [
        ('junior', 'มัธยมต้น'),
        ('senior', 'มัธยมปลาย'),
        ('high-vocational', 'ประกาศนียบัตรวิชาชีพชั้นสูง'),
        ('technical-certificate', 'ประกาศนียบัตรวิชาชีพเทคนิค'),
        ('non-formal', 'การศึกษานอกโรงเรียน'),
        ('online', 'คอร์สออนไลน์'),
        ('bachelor', 'ปริญญาตรี'),
        ('master', 'ปริญญาโท'),
        ('phd', 'ปริญญาเอก'),
    ]
    CHOICES_TH_DICT = dict(CHOICES_TH)

    GRADE_CHOICES = [
        ('grade', 'Grade with Score'),
        ('pf', 'Only Pass or Fail'),
    ]
    GRADE_CHOICES_DICT = dict(GRADE_CHOICES)

    GRADE_CHOICES_TH = [
        ('grade', 'ระบบเกรด'),
        ('pf', 'ระบบผ่านและไม่ผ่าน'),
    ]
    GRADE_CHOICES_TH_DICT = dict(GRADE_CHOICES_TH)

    PF_CHOICES = [
        ('pass', 'Pass'),
        ('fail', 'Fail'),
    ]
    PF_CHOICES_DICT = dict(PF_CHOICES)

    PF_CHOICES_TH = [
        ('pass', 'ผ่าน'),
        ('fail', 'ไม่ผ่าน'),
    ]
    PF_CHOICES_TH_DICT = dict(PF_CHOICES_TH)

    institution = models.CharField(
        max_length=100, help_text="Name of institution that you involve")

    area = models.CharField(
        max_length=100, help_text="Area or courses name")

    type = models.CharField(
        max_length=30, choices=CHOICES, help_text="Type of the education")

    grade_type = models.CharField(
        max_length=30, choices=GRADE_CHOICES,
        blank=True, null=True, help_text="Type of the education")

    score = models.DecimalField(
        max_digits=5, decimal_places=2,
        help_text="Score of the overall courses or individual course")

    pass_or_fail = models.CharField(
        max_length=15, choices=PF_CHOICES,
        blank=True, null=True, help_text="Do you pass or fail the courses")

    certificate = CloudinaryField(
        blank=True, null=True, resource_type='auto',
        help_text="certificate that proof you actually learn the courses")

    def translate_type(self, lang):
        if lang == 'th':
            return self.CHOICES_TH_DICT[self.type]
        else:
            return self.CHOICES_DICT[self.type]

    def translate_grade_type(self, lang):
        if lang == 'th':
            return self.GRADE_CHOICES_TH_DICT[self.grade_type]
        else:
            return self.GRADE_CHOICES_DICT[self.grade_type]

    def translate_pass_or_fail(self, lang):
        if lang == 'th':
            return self.PF_CHOICES_TH_DICT[self.pass_or_fail]
        else:
            return self.PF_CHOICES_DICT[self.pass_or_fail]

    class TranslatableMeta:
        fields = ['institution', 'area']

    def __str__(self):
        return "{} ({})".format(self.area, self.institution)


auditlog.register(Education)
