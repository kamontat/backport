from collections import OrderedDict

from django.conf import settings

from rest_framework import serializers

import cloudinary
cloudinary.config(secure=True)


class CloudinarySerializer(serializers.Serializer):
    cloud_name = serializers.CharField(max_length=50, required=False)
    resource_type = serializers.CharField(max_length=20)
    type = serializers.CharField(max_length=20)
    version = serializers.CharField(max_length=100)
    public_id = serializers.CharField(max_length=100)
    format = serializers.CharField(max_length=20)
    url = serializers.URLField()

    def to_representation(self, obj=None):
        i = super().to_representation(obj)
        i['cloud_name'] = cloudinary.config().cloud_name
        return {
            'cloud_name': i['cloud_name'],
            'resource_type': i['resource_type'],
            'type': i['type'],
            'version': i['version'],
            'public_id': i['public_id'],
            'format': i['format'],
            'url': i['url'],
        }


class TranslatableListSerializer(serializers.ListSerializer):
    def set_lang(self, lang):
        self.child.set_lang(lang)

    def get_lang(self):
        return self.child.get_lang()


class BaseSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        result = super().to_representation(instance)
        if isinstance(result, str):
            return result
        return OrderedDict([(key, result[key]) for key in result if result[key] is not None])


class WithVersionKey(serializers.ModelSerializer):
    def to_representation(self, instance):
        orderdict = super().to_representation(instance)
        orderdict.update({'version': settings.VERSION})
        orderdict.move_to_end('version', last=False)
        return orderdict


class WithTranslatable(serializers.ModelSerializer):
    """
    Custom and self define translatable fields using translate method
    """
    current_lang = 'en'

    def to_representation(self, instance):
        result = super().to_representation(instance)
        return self.translate(result, instance)

    def set_lang(self, lang):
        self.current_lang = lang

    def get_lang(self):
        return self.current_lang

    def translate(self, return_object, instance):
        raise NotImplementedError(
            "Subclass of Translatable Serializer must implemented translate method"
            "Default, this method should receive instance of object and return transformed object."
        )

    @classmethod
    def many_init(cls, *args, **kwargs):
        kwargs['child'] = cls()
        return TranslatableListSerializer(*args, **kwargs)


class WithCloudinary(serializers.ModelSerializer):
    cloudinary_fields = None

    def to_representation(self, instance):
        results = super().to_representation(instance)
        for field in self.cloudinary_fields:
            value = results.pop(field, None)
            if value != None:
                results[field] = CloudinarySerializer(
                    getattr(instance, field)).data

        return results


class WithOnlyKey(serializers.ModelSerializer):
    only_field = None

    def to_representation(self, instance):
        if self.only_field == None:
            raise NotImplementedError(
                'To use with no key serializer', 'you must specify only_field name')

        result = super().to_representation(instance)
        if result[self.only_field] == None or result[self.only_field] == '':
            return ''

        return result[self.only_field]


class WithCustomObject(serializers.ModelSerializer):
    def to_representation(self, instance):
        result = super().to_representation(instance)
        result.update(self.make_object(instance))

        return result

    def make_object(self, instance):
        raise NotImplementedError(
            'You must implement make_object()',
            'which take instance of object ',
            'return new json that contain new key and value',
            'this method will merge with default return object')
