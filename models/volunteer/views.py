from api import views

from api.filters import WithOwner, WithPageOrder

from .models import Volunteer
from .serializers import VolunteerSerializer

from models.experience.views import ExperienceViewSet


class VolunteerFilter(WithOwner, WithPageOrder):
    """
    Filter volunteer project in several format and columns
    """

    class Meta:
        model = Volunteer
        fields = ['who', 'technical', 'tag']


class VolunteerViewSet(ExperienceViewSet):
    """
    API endpoint that allows users able to get volunteers for specify personal information
    """

    queryset = Volunteer.objects.filter(
        page_order__gte=0).order_by('-start_date', '-end_date')
    serializer_class = VolunteerSerializer
    filter_class = VolunteerFilter
