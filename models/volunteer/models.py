from auditlog.registry import auditlog

from django.db import models
from models.experience.models import Experience
from cloudinary.models import CloudinaryField


class Volunteer(Experience):
    """
    This quite similar to works except this didn't earn any money
    """
    organization = models.CharField(
        max_length=100, help_text="organization of the volunteer")
    position = models.CharField(
        max_length=100, help_text="position on that organization")
    website = models.URLField(
        blank=True, null=True,
        help_text="A website of organization or event")

    class TranslatableMeta:
        fields = Experience.get_translatable_meta() + ('organization', 'position')

    class Meta:
        app_label = 'experience'

    def __str__(self):
        return "{} ({})".format(self.position, self.organization)


auditlog.register(Volunteer)
