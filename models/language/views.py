from api import views

from api.filters import WithOwner
from .serializers import Language, LanguageSerializer


class LanguageFilter(WithOwner):
    class Meta:
        model = Language
        fields = ['who']


class LanguageViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of user languages with specify personal information
    """

    required_queries = ['who']
    queryset = Language.objects.order_by('id')
    serializer_class = LanguageSerializer
    filter_class = LanguageFilter
