from rest_framework.test import APITestCase
from rest_framework import status


class InterestTest(APITestCase):
    def test_list_all_skill(self):
        """
        Ensure we cannot list all available skill
        """

        response = self.client.get('/v1/interests/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
