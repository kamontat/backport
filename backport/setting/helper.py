import os
import re


def get_env_variable(var_name):
    """ Get the environment variable or return exception """
    print("Start loading...  ", end='')
    try:
        env_var = os.environ[var_name]
        first = env_var[:5]
        other = re.sub('.', ".", env_var[5:])

        print("[ ] | {:25s} = {}".format(var_name, first + other))
        return env_var
    except KeyError:
        print("[E] | {:25s} = ''".format(var_name))
        return ''
