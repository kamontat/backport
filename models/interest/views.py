from api import views

from api.filters import BaseFilter, WithOwner, WithPageOrder
from .serializers import Interest, InterestSerializer


class InterestFilter(BaseFilter, WithOwner, WithPageOrder):
    class Meta:
        model = Interest
        fields = ['who']


class InterestViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of interests in specify personal information
    """

    required_queries = ['who']
    translate_fields = ['tag']
    queryset = Interest.objects.order_by('id')
    serializer_class = InterestSerializer
    filter_class = InterestFilter
