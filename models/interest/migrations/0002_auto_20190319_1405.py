# Generated by Django 2.0.8 on 2019-03-19 07:05

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('interest', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='interest',
            managers=[
                ('inheritance', django.db.models.manager.Manager()),
            ],
        ),
    ]
