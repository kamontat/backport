from rest_framework.test import APITestCase
from rest_framework import status


class KeywordTest(APITestCase):
    def test_list_tags(self):
        """
        Ensure we can list all availble tags
        """

        response = self.client.get('/v1/list/tags/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_technical(self):
        """
        Ensure we can list all availble technical keyword
        """

        response = self.client.get('/v1/list/technicals/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_all(self):
        """
        Ensure we can list all tags and technical in one query
        """

        response = self.client.get('/v1/list/all/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)