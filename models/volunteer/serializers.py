from api.serializers import BaseSerializer

from models.experience.serializers import ExperienceSerializer

from .models import Volunteer


class VolunteerSerializer(ExperienceSerializer):

    class Meta:
        model = Volunteer
        fields = ('id', 'banner', 'organization', 'position', 'website',
                  'start_date', 'end_date', 'tag', 'technical', 'summary', 'body',
                  'gallery', 'highlights', 'page_order', 'created_at', 'updated_at')


class MiniVolunteerSerializer(ExperienceSerializer):
    class Meta:
        model = Volunteer
        fields = ('id', 'banner', 'organization', 'position', 'summary')
