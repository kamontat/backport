import os

from backport.setting.helper import get_env_variable

from backport.application import *

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

GAE_VERSION = get_env_variable('GAE_VERSION')

if GAE_VERSION != "":
    gae = GAE_VERSION.split('t')

    BUILD_YEAR = int(gae[0][:4])
    BUILD_MONTH = int(gae[0][4:6])
    BUILD_DAY = int(gae[0][6:])
    BUILD_HOUR = int(gae[1][:2])
    BUILD_MINUTE = int(gae[1][2:4])
    BUILD_SECOND = int(gae[1][4:])

# keep the secret key used in production secret!
SECRET_KEY = get_env_variable('SECRET_KEY')

# don't run with debug turned on in production!
DEBUG = get_env_variable('PRODUCTION') == "false"

PREFIX = ""
if DEBUG:
    PREFIX = "-dev"

VERSION = "{}{}".format(VERSION, PREFIX)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/
STATIC_ROOT = 'static'
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "public"),
]

# Cloudinary settings for Django. Add to your settings file.
CLOUDINARY = {
    'cloud_name': get_env_variable('CLOUDINARY_CLOUD_NAME'),
    'api_key': get_env_variable('CLOUDINARY_API_KEY'),
    'api_secret': get_env_variable('CLOUDINARY_API_SECRET'),
}
