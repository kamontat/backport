from api import views
from .models import Tag, Technical
from .serializers import TagSerializer, TechnicalSerializer


class TagViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of tag available in website
    """

    queryset = Tag.objects.order_by('name')
    serializer_class = TagSerializer


class TechnicalViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of technical available in website
    """

    queryset = Technical.objects.order_by('name')
    serializer_class = TechnicalSerializer


class TagAndTechnicalViewSet(views.WithMultipleModel, views.IncludeListAPI):
    querylist = [
        {'label': 'tags', 'queryset': Tag.objects.order_by(
            'name'), 'serializer_class': TagSerializer},
        {'label': 'technicals', 'queryset': Technical.objects.order_by(
            'name'), 'serializer_class': TechnicalSerializer},
    ]
