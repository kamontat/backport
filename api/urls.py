from django.urls import path, include

from rest_framework.routers import DefaultRouter

from models.personal.views import PersonalViewSet, SocialViewSet

from models.education.views import EducationViewSet
from models.work.views import WorkViewSet
from models.project.views import ProjectViewSet
from models.volunteer.views import VolunteerViewSet

from models.language.views import LanguageViewSet
from models.skill.views import SkillViewSet
from models.interest.views import InterestViewSet
from models.reference.views import ReferenceViewSet

from models.tag.views import TagViewSet, TechnicalViewSet, TagAndTechnicalViewSet

from summary.views import SummaryViewSet

router = DefaultRouter()

router.register(r'information', PersonalViewSet)
router.register(r'socials', SocialViewSet)

router.register(r'educations', EducationViewSet)
router.register(r'works', WorkViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'volunteers', VolunteerViewSet)

router.register(r'languages', LanguageViewSet)
router.register(r'skills', SkillViewSet)
router.register(r'interests', InterestViewSet)
router.register(r'references', ReferenceViewSet)

router.register(r'list/tags', TagViewSet)
router.register(r'list/technicals', TechnicalViewSet)
router.register(r'list/all', TagAndTechnicalViewSet,
                basename="tag-and-technical-list")

router.register(r'make/summary', SummaryViewSet, basename="summary")

urlpatterns = [
    path('', include(router.urls)),
]
