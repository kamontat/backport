from django.utils.translation import ugettext_lazy as _

from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard, AppIndexDashboard
from jet.dashboard.dashboard_modules import google_analytics


class MyDashboard(Dashboard):
    columns = 3

    def init_with_context(self, context):
        self.available_children.append(
            google_analytics.GoogleAnalyticsVisitorsTotals)

        self.available_children.append(
            google_analytics.GoogleAnalyticsVisitorsChart)

        self.available_children.append(
            google_analytics.GoogleAnalyticsPeriodVisitors)

        self.available_children.append(modules.Feed)

        self.children.append(modules.AppList(
            _('Administration'),
            models=('auth.*',),
            column=1,
            order=0)
        )

        self.children.append(modules.AppList(
            _('Auditlog'),
            models=('auditlog.*',),
            column=1,
            order=1)
        )

        self.children.append(modules.AppList(
            _('Models'),
            exclude=('auth.*', 'auditlog.*'),
            column=1,
            order=2)
        )

        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=20,
            column=2,
            order=0
        ))

        # self.children.append(modules.Feed(
        #     _('Latest Django News'),
        #     feed_url='http://www.djangoproject.com/rss/weblog/',
        #     limit=3,
        #     column=2,
        #     order=1
        # ))

        self.children.append(modules.Feed(
            _("Posttoday News"),
            feed_url='http://www.posttoday.com/rss/src/breakingnews.xml',
            limit=4,
            column=2,
            order=1
        ))
