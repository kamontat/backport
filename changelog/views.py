import os
import markdown

from django.views.generic import TemplateView


class ChangelogView(TemplateView):
    template_name = "changelog.html"

    def get_context_data(self, **kwargs):
        markdowntext = open(os.path.join(
            os.path.dirname(__file__), '../CHANGELOG.md')).read()

        context = super().get_context_data(**kwargs)
        context['markdowntext'] = markdown.markdown(markdowntext)

        return context
