from datetime import datetime

from django.conf import settings

from rest_framework.views import APIView
from rest_framework.response import Response


class VersionView(APIView):
    """
    get:
    API endpoint that query current APIs version
    """

    def get(self, request, format=None):
        info = {
            'version': settings.VERSION,
            'developer': {
                'name': settings.DEVELOPER,
                'url': settings.DEVELOPER_URL
            }
        }

        if settings.GAE_VERSION:
            info['date'] = datetime(settings.BUILD_YEAR, settings.BUILD_MONTH, settings.BUILD_DAY,
                                    settings.BUILD_HOUR, settings.BUILD_MINUTE, settings.BUILD_SECOND)
        return Response(info)
