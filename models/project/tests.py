from rest_framework.test import APITestCase
from rest_framework import status


class ProjectTest(APITestCase):
    def test_list_all_projects(self):
        """
        Ensure we cannot list all project
        """

        response = self.client.get('/v1/projects/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
