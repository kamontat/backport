from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithAutoOrderField
from .models import Tag, Technical


class TagAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithAutoOrderField):
    pass


class TechnicalAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithAutoOrderField):
    pass


admin.site.register(Tag, TagAdmin)
admin.site.register(Technical, TechnicalAdmin)
