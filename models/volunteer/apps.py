from django.apps import AppConfig


class VolunteerConfig(AppConfig):
    name = 'models.volunteer'
