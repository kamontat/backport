from django.apps import AppConfig


class LanguageConfig(AppConfig):
    name = 'models.language'
