from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithPageOrder, WithAutoOrderField
from .models import Interest


class InterestAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithPageOrder, WithPerson, WithAutoOrderField):
    search_fields = ['name']


admin.site.register(Interest, InterestAdmin)
