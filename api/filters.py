import rest_framework_filters as filters


class BaseFilter(filters.FilterSet):
    pass


class WithOwner(filters.FilterSet):
    who = filters.CharFilter(
        help_text="A person who be the owner of the result"
    )


class WithPageOrder(filters.FilterSet):
    is_ascend = filters.BooleanFilter(
        field_name='page_order', method='filter_ascend', help_text="Order the result, Default is ascending")

    def filter_ascend(self, qs, name, value):
        order = '-page_order'
        if value == True:
            order = 'page_order'

        return qs.order_by(order)
