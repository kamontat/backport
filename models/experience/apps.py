from django.apps import AppConfig


class ExperienceConfig(AppConfig):
    name = 'models.experience'
