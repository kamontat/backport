from django.utils.translation import gettext_lazy as _

from backport.setting.variable import DEBUG

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

TIME_ZONE = 'Asia/Bangkok'

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('en', _('English')),
    ('th', _('Thai')),
)

USE_I18N = True

USE_L10N = True

USE_TZ = True
