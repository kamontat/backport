from django.urls import reverse

from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view()
def indexPage(request):
    return Response({
        'page': 'index',
        'lists': [
            {
                'page': 'docs',
                'url': request.build_absolute_uri('/docs')
            }, {
                'page': 'admin',
                'url': request.build_absolute_uri('/admin')
            }, {
                'page': 'admin-docs',
                'url': request.build_absolute_uri('/admin/docs')
            }, {
                'page': 'apis-v1',
                'url': request.build_absolute_uri('/v1/')
            }, {
                'page': 'version',
                'url': request.build_absolute_uri('/version/')
            }, {
                'page': 'changelog',
                'url': request.build_absolute_uri('/changelog/')
            }
        ]
    })
