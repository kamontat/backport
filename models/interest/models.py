from auditlog.registry import auditlog

from django.db import models
from api.models import BaseModel, WithTranslatable, WithPerson, WithPageOrder, WithTagable


class Interest(BaseModel, WithPageOrder, WithPerson, WithTranslatable, WithTagable):
    name = models.CharField(
        max_length=100, help_text="Interest name")

    class TranslatableMeta:
        fields = ('name',)

    def __str__(self):
        return "{}".format(self.name)


auditlog.register(Interest)
