from auditlog.registry import auditlog

from django.db import models

from api.models import BaseModel, WithPerson, WithTranslatable, WithPageOrder

from cloudinary.models import CloudinaryField


class Reference(BaseModel, WithPerson, WithTranslatable, WithPageOrder):
    referee = models.CharField(
        max_length=100, help_text="the person who give a recommendation to you")

    referee_link = models.URLField(
        blank=True, null=True, help_text="profile link of referee (e.g. linkedin or personal website)")

    attachment = CloudinaryField(
        blank=True, null=True, resource_type='auto',
        help_text="attachment (in case text is not provided)")

    raw_text = models.TextField(
        max_length=200, help_text="Reference text or attachment", blank=True, null=True)

    link = models.URLField(
        blank=True, null=True, help_text="Link of the reference if reference is published")

    page_order = models.SmallIntegerField(
        default=0, blank=True, null=True,
        help_text="A order of the response JSON (negative to excluded)")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class TranslatableMeta:
        fields = ('raw_text', 'attachment', 'link')

    def __str__(self):
        return "{}".format(self.referee)


auditlog.register(Reference)
