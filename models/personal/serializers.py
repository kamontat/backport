from django.urls import reverse

from rest_framework import serializers

from api.serializers import BaseSerializer, WithVersionKey, WithCloudinary, WithCustomObject

from .models import Personal, Social


class PersonalSerializer(BaseSerializer, WithVersionKey, WithCloudinary, WithCustomObject):
    cloudinary_fields = ['image']

    class Meta:
        model = Personal
        fields = ('id', 'image', 'firstname',
                  'lastname', 'nickname', 'email', 'birthday', 'phone_number', 'myself', 'organization', 'summary', 'created_at', 'updated_at')

    def make_object(self, instance):
        request = self.context['request']

        person_id = instance.id

        def build_url(request, username, namespace):
            return '{}?who={}'.format(request.build_absolute_uri(
                reverse(namespace)), username)

        response = {
            'social_url': build_url(request, person_id, 'social-list'),
            'education_url': build_url(request, person_id, 'education-list')
        }

        if instance.address_name != '':
            response.update({
                'address': {
                    'name': instance.address_name,
                    'name2': instance.address_name2,
                    'city': instance.address_city,
                    'country': instance.address_country,
                    'postal_code': instance.address_postal_code,
                }
            })

        return response


class SocialSerializer(BaseSerializer):
    class Meta:
        model = Social
        fields = ('network', 'username', 'url', 'created_at', 'updated_at')


class MiniSocialSerializer(BaseSerializer):
    class Meta:
        model = Social
        fields = ('network', 'username', 'url')
