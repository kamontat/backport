from django.conf import settings
from django.db.models.query import QuerySet

from rest_framework import mixins, viewsets, status
from rest_framework.response import Response

from rest_framework_extensions.mixins import PaginateByMaxMixin
from rest_framework_extensions.cache.mixins import RetrieveCacheResponseMixin, ListCacheResponseMixin

from .exceptions import InvalidQuery
from .serializers import TranslatableListSerializer
from .pagination import MultipleModelLimitOffsetPagination


class BaseLocaleViewAPI(PaginateByMaxMixin, viewsets.GenericViewSet):
    translate_fields = None
    required_queries = None

    max_paginate_by = 100
    max_page_size = 100

    def get_queryset(self):
        qs = super().get_queryset()
        return self.update_queryset(qs, self.request.GET.get('lang'))

    def update_queryset(self, qs, language):
        try:
            if self.translate_fields:
                return qs.translate(language).translate_related(*self.translate_fields)
            else:
                return qs.translate(language)
        except Exception as e:
            print('ERROR: {}'.format(e))
            return qs

    def get_serializer(self, *args, **kwargs):
        serializer = super().get_serializer(*args, **kwargs)
        if issubclass(serializer.__class__, TranslatableListSerializer):
            serializer.set_lang(self.request.GET.get('lang'))

        return serializer

    def check_require_queries(self, request):
        if self.required_queries:
            for query in self.required_queries:
                if request.GET.get(query) == None:
                    raise InvalidQuery(
                        'query ({}) is mark as required'.format(query))
        return None


class IncludeListAPI(mixins.ListModelMixin, ListCacheResponseMixin, BaseLocaleViewAPI):

    def before_get_response(self, request):
        error_response = self.check_require_queries(request)
        if error_response != None:
            return error_response
        else:
            return None

    def list(self, request, *args, **kwargs):
        error_response = self.before_get_response(request)
        if error_response:
            return error_response

        response = super().list(request, args, kwargs)
        response['Version'] = settings.VERSION
        return response


class IncludeRetrieveAPI(mixins.RetrieveModelMixin, RetrieveCacheResponseMixin, BaseLocaleViewAPI):
    def retrieve(self, request, *args, **kwargs):
        error_response = self.check_require_queries(request)
        if error_response != None:
            return error_response

        response = super().retrieve(request, args, kwargs)
        response['Version'] = settings.VERSION
        return response


class IncludeUpdateAPI(mixins.UpdateModelMixin, BaseLocaleViewAPI):

    def update(self, request, *args, **kwargs):
        response = super().update(request, args, kwargs)
        response['Version'] = settings.VERSION
        return response


class IncludeCreateAPI(mixins.UpdateModelMixin, BaseLocaleViewAPI):

    def create(self, request, *args, **kwargs):
        response = super().create(request, args, kwargs)
        response['Version'] = settings.VERSION
        return response


class IncludeDestroyAPI(mixins.DestroyModelMixin, BaseLocaleViewAPI):

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, args, kwargs)
        response['Version'] = settings.VERSION
        return response


class WithMultipleModel(object):
    """
    Base class that holds functions need for all MultipleModelMixins/Views
    credit: https://github.com/MattBroach/DjangoRestMultipleModels/blob/893969ed38d614a5e2f060e560824fa7c5c49cfd/drf_multiple_model/mixins.py#L8-L132
    """

    querylist = None
    result_type = dict
    pagination_class = MultipleModelLimitOffsetPagination

    # Keys required for every item in a querylist
    required_keys = ['queryset', 'serializer_class']

    # default pagination state. Gets overridden if pagination is active
    is_paginated = False

    def get_querylist(self):
        assert self.querylist is not None, (
            '{} should either include a `querylist` attribute, '
            'or override the `get_querylist()` method.'.format(
                self.__class__.__name__
            )
        )

        return self.querylist

    def check_query_data(self, query_data):
        """
        All items in a `querylist` must at least have `queryset` key and a
        `serializer_class` key. Any querylist item lacking both those keys
        will raise a ValidationError
        """
        for key in self.required_keys:
            if key not in query_data:
                raise ValidationError(
                    'All items in the {} querylist attribute should contain a '
                    '`{}` key'.format(self.__class__.__name__, key)
                )

    def load_queryset(self, query_data, request, *args, **kwargs):
        """
        Fetches the queryset and runs any necessary filtering, both
        built-in rest_framework filters and custom filters passed into
        the querylist
        """

        qs = query_data.get('queryset', [])
        try:
            qs = self.update_queryset(qs, request.GET.get('lang'))
            # Method exists and was used.
        except AttributeError:
            pass

        if isinstance(qs, QuerySet):
            qs = qs.all()

        # run rest_framework filters
        qs = self.filter_queryset(qs)

        # run custom filters
        filter_fn = query_data.get('filter_fn', None)
        if filter_fn is not None:
            qs = filter_fn(qs, request, *args, **kwargs)

        page = self.paginate_queryset(qs)
        self.is_paginated = page is not None

        return page if page is not None else qs

    def get_empty_results(self):
        """
        Because the base result type is different depending on the return structure
        (e.g. list for flat, dict for object), `get_result_type` initials the
        `results` variable to the proper type
        """
        assert self.result_type is not None, (
            '{} must specify a `result_type` value or overwrite the '
            '`get_empty_result` method.'.format(self.__class__.__name__)
        )

        return self.result_type()

    def add_to_results(self, data, label, results):
        """
        responsible for updating the running `results` variable with the
        data from this queryset/serializer combo
        """
        results[label] = data

        return results

    def format_results(self, results, request):
        """
        hook for processing/formatting the entire returned data set, once
        the querylist has been evaluated
        """
        return results

    def get_label(self, queryset, query_data):
        """
        Gets option label for each datum. Can be used for type identification
        of individual serialized objects
        """
        if query_data.get('label', False):
            return query_data['label']

        try:
            return queryset.model.__name__
        except AttributeError:
            return query_data['queryset'].model.__name__

    def get_queryset(self):
        return None

    def list(self, request, *args, **kwargs):
        error_response = self.before_get_response(request)
        if error_response:
            return error_response

        querylist = self.get_querylist()
        results = self.get_empty_results()

        for query_data in querylist:
            self.check_query_data(query_data)

            queryset = self.load_queryset(query_data, request, *args, **kwargs)

            # Run the paired serializer
            context = self.get_serializer_context()

            # get serializer class
            self.serializer_class = query_data['serializer_class']
            # serialize and receive the data
            data = self.get_serializer(
                queryset, many=True, context=context).data

            label = self.get_label(queryset, query_data)

            # Add the serializer data to the running results tally
            results = self.add_to_results(data, label, results)

        formatted_results = self.format_results(results, request)

        if self.is_paginated:
            try:
                formatted_results = self.paginator.format_response(
                    formatted_results)
            except AttributeError:
                raise NotImplementedError(
                    "{} cannot use the regular Rest Framework or Django paginators as is. "
                    "Use one of the included paginators from `drf_multiple_models.pagination "
                    "or subclass a paginator to add the `format_response` method."
                    "".format(self.__class__.__name__)
                )

        return Response(formatted_results, status=status.HTTP_200_OK, headers={'Version': settings.VERSION})
