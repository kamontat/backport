from auditlog.registry import auditlog

from django.db import models
from api.models import BaseModel, WithTranslatable, WithPerson, WithPageOrder, WithTagable


class Skill(BaseModel, WithTranslatable, WithPageOrder, WithPerson, WithTagable):
    LEVEL_CHOICES = (
        ("beginner", "Beginner"),
        ("basic", "Basic"),
        ("intermediate", "Intermediate"),
        ("advanced", "Advanced",),
        ("superior", "Superior",),
    )
    LEVEL_CHOICES_DICT = dict(LEVEL_CHOICES)

    LEVEL_CHOICES_TH = (
        ("beginner", "ระดับเริ่มต้น"),
        ("basic", "ระดับพื้นฐาน"),
        ("intermediate", "ระดับกลาง"),
        ("advanced", "ระดับสูง",),
        ("superior", "ผู้เชี่ยวชาญ",),
    )
    LEVEL_CHOICES_TH_DICT = dict(LEVEL_CHOICES_TH)

    name = models.CharField(
        max_length=100, help_text="Skill name")
    description = models.CharField(
        max_length=100, help_text="Skill description", blank=True, null=True)
    level = models.CharField(
        max_length=100, help_text="How good for you on the skill", choices=LEVEL_CHOICES)

    def translate_level(self, lang):
        if lang == 'th':
            return self.LEVEL_CHOICES_TH_DICT[self.level]
        else:
            return self.LEVEL_CHOICES_DICT[self.level]

    class TranslatableMeta:
        fields = ('name', 'description',)

    def __str__(self):
        return "{} ({})".format(self.name, self.level)


auditlog.register(Skill)
