from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithPageOrder, WithAutoOrderField
from .models import Skill


class SkillAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithPageOrder, WithAutoOrderField):
    list_display = ('name',)


admin.site.register(Skill, SkillAdmin)
