from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithAutoOrderField
from .models import Language


class LanguageAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithAutoOrderField):
    list_display = ('name', 'proficiency')
    search_fields = ('name',)


admin.site.register(Language, LanguageAdmin)
