from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithPageOrder, WithAutoOrderField
from .models import Education


class EducationAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithPageOrder, WithPerson, WithAutoOrderField):
    list_display = ('institution', 'area', 'type')

    fields = ('institution', 'area', 'type', 'start_date', "end_date",
              'grade_type', 'score', 'pass_or_fail', 'certificate')


admin.site.register(Education, EducationAdmin)
