from django.apps import AppConfig


class InterestConfig(AppConfig):
    name = 'models.interest'
