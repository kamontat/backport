from rest_framework.test import APITestCase
from rest_framework import status


class SkillTest(APITestCase):
    def test_list_all_skill(self):
        """
        Ensure we can list all available skill
        """

        response = self.client.get('/v1/skills/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
