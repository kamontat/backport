from api.serializers import BaseSerializer

from .models import Reference


class ReferenceSerializer(BaseSerializer):
    class Meta:
        model = Reference
        fields = ('referee', 'referee_link', 'attachment', 'raw_text', 'link',
                  'page_order', 'created_at', 'updated_at')


class MiniReferenceSerializer(BaseSerializer):
    class Meta:
        model = Reference
        fields = ('referee', 'attachment', 'raw_text', 'link')
