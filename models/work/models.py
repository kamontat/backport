from auditlog.registry import auditlog

from django.db import models
from models.experience.models import Experience


class Work(Experience):
    company = models.CharField(
        max_length=100, help_text="A company name")
    position = models.CharField(
        max_length=100, help_text="A work position in the company")
    website = models.URLField(
        blank=True, null=True,
        help_text="A website of company or more information to the company")

    class TranslatableMeta:
        fields = Experience.get_translatable_meta() + ('company', 'position')

    class Meta:
        app_label = 'experience'

    def __str__(self):
        return "{} ({})".format(self.position, self.company)


auditlog.register(Work)
