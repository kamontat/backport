from django.apps import AppConfig


class EducationConfig(AppConfig):
    name = 'models.education'
