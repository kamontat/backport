from api.serializers import BaseSerializer

from .models import Interest


class InterestSerializer(BaseSerializer):
    class Meta:
        model = Interest
        fields = ('name', 'tag', 'page_order', 'created_at', 'updated_at')


class MiniInterestSerializer(BaseSerializer):
    class Meta:
        model = Interest
        fields = ('name',)
