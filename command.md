## Database
1. Start proxy to google sql - `cloud_sql_proxy -instances="backport:asia-east2:portdata"=tcp:3306`

## django command

1. Run server - `python manage.py runserver`
2. Create migration - `python manage.py makemigrations`
3. Apply migrate - `python manage.py migrate`