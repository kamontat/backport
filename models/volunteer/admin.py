from models.experience.abstract.admin import ExperienceAdmin
from .models import Volunteer


class VolunteerAdmin(ExperienceAdmin):
    experience_title_fields = (('organization', 'position'), 'website')
    list_display = ('organization', 'position')
