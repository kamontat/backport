from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithTranslatable, WithPerson, WithPageOrder, WithAutoOrderField

from models.experience.models import Highlight, Experience


class TagInline(admin.TabularInline):
    verbose_name = "Tag"
    verbose_name_plural = "Tags"
    model = Experience.tag.through
    extra = 2


class TechnicalInline(admin.TabularInline):
    verbose_name = "Technical"
    verbose_name_plural = "Technicals"
    model = Experience.technical.through
    extra = 2


class HighlightInline(admin.TabularInline):
    model = Highlight


class ExperienceAdmin(BaseAdmin, ExtendAdminView, WithTranslatable, WithPageOrder, WithPerson, WithAutoOrderField):
    experience_title_fields = None

    exclude = ('tag', 'technical')
    inlines = (HighlightInline, TagInline, TechnicalInline,)

    def get_fieldsets(self, request, obj=None):
        if self.experience_title_fields == None:
            raise EnvironmentError(
                'experience_title_fields is required for experience admin')

        main_fields = ('id', 'who', 'banner',) + self.experience_title_fields + (
            ('start_date', 'end_date'),
            'summary',
            'gallery',
            'page_order',
            ('created_at', 'updated_at'))

        return (
            (None, {
                'fields': main_fields
            }),
            ('Body', {
                'fields': ('body', ),
            }),
        )
