from django.db.models import Q

from rest_framework.response import Response
from rest_framework import status as http_status

from api import views
from api.filters import WithOwner, WithPageOrder

from api.exceptions import InvalidQuery

from models.personal.serializers import Social, MiniSocialSerializer

from models.education.serializers import Education, MiniEducationSerializer
from models.language.serializers import Language, MiniLanguageSerializer

from models.reference.serializers import Reference, MiniReferenceSerializer

from models.skill.serializers import Skill, MiniSkillSerializer
from models.interest.serializers import Interest, MiniInterestSerializer

from models.project.serializers import Project, MiniProjectSerializer
from models.volunteer.serializers import Volunteer, MiniVolunteerSerializer
from models.work.serializers import Work, MiniWorkSerializer


class SummaryViewSet(views.WithMultipleModel, views.IncludeListAPI):
    """
    Summary all information from another query
    """

    required_queries = ['who']

    support_queries = [
        'social', 'skill', 'interest',
        'work', 'volunteer', 'project',
        'education', 'reference', 'language',
    ]

    def check_filter(self, filters):
        for filter in filters:
            if filter not in self.support_queries:
                return False

        return True

    def get_url_filter(self, get_url_query, filter_name):
        filter = get_url_query(filter_name)
        if filter:
            filter = filter.split(',')
            if not self.check_filter(filter):
                raise InvalidQuery(
                    detail="'{}' is support in {} list".format(filter_name, self.support_queries))

        return filter

    def get_querylist(self):
        get_url_query = self.request.GET.get

        lang = get_url_query('lang')
        who = get_url_query('who')
        tag = get_url_query('tag')

        def filter_tag(qs, tag, lang):
            if tag == None:
                return qs
            else:
                # must be exact to avoid any query mistake
                new_qs = qs.probe(
                    lang).filter(
                    Q(technical__name__iexact=tag) |
                    Q(tag__name__iexact=tag)).distinct()

                return new_qs

        def filter_only_tag(qs, tag, lang):
            if tag == None:
                return qs
            else:
                # must be exact to avoid any query mistake
                return qs.probe(
                    lang).filter(
                    Q(tag__name__iexact=tag)).distinct()

        only = self.get_url_filter(get_url_query, 'only')
        exclude = self.get_url_filter(get_url_query, 'exclude')

        results = {
            'social': {
                'label': 'socials',
                'queryset': Social.objects.filter(who=who),
                'serializer_class': MiniSocialSerializer
            },
            'education': {
                'label': 'educations',
                'queryset': Education.objects.filter(who=who),
                'serializer_class': MiniEducationSerializer
            },
            'skill': {
                'label': 'skills',
                'queryset': filter_only_tag(Skill.objects.filter(who=who), tag, lang),
                'serializer_class': MiniSkillSerializer
            },
            'project': {
                'label': 'projects',
                'queryset': filter_tag(Project.objects.filter(who=who), tag, lang),
                'serializer_class': MiniProjectSerializer
            },
            'work': {
                'label': 'works',
                'queryset': filter_tag(Work.objects.filter(who=who), tag, lang),
                'serializer_class': MiniWorkSerializer
            },
            'volunteer': {
                'label': 'volunteers',
                'queryset': filter_tag(Volunteer.objects.filter(who=who), tag, lang),
                'serializer_class': MiniVolunteerSerializer
            },
            'reference': {
                'label': 'references',
                'queryset': Reference.objects.filter(who=who),
                'serializer_class': MiniReferenceSerializer
            },
            'language': {
                'label': 'languages',
                'queryset': Language.objects.filter(who=who),
                'serializer_class': MiniLanguageSerializer
            },
            'interest': {
                'label': 'interests',
                'queryset': filter_only_tag(Interest.objects.filter(who=who), tag, lang),
                'serializer_class': MiniInterestSerializer
            }
        }

        if only:
            return [results[filter_field] for filter_field in only]

        if exclude:
            return [value for key, value in results.items() if key not in exclude]

        return results.values()
