from django.contrib import admin

from api.admin import BaseAdmin, ExtendAdminView, WithAutoOrderField
from .models import EPhoto, Highlight, Experience

from models.volunteer.admin import Volunteer, VolunteerAdmin
from models.work.admin import Work, WorkAdmin
from models.project.admin import Project, ProjectAdmin


class ExperienceInline(admin.TabularInline):
    verbose_name = "Experience"
    verbose_name_plural = "Experiences"
    model = EPhoto.experience.through
    extra = 1


class PhotoAdmin(BaseAdmin, ExtendAdminView, WithAutoOrderField):
    inlines = (ExperienceInline,)

    list_display = ('name', 'a_link')
    search_fields = ('name',)


admin.site.register(EPhoto, PhotoAdmin)

admin.site.register(Work, WorkAdmin)
admin.site.register(Volunteer, VolunteerAdmin)
admin.site.register(Project, ProjectAdmin)
