from api import views

from api.filters import WithOwner, WithPageOrder
from .serializers import Skill, SkillSerializer


class SkillFilter(WithOwner, WithPageOrder):
    class Meta:
        model = Skill
        fields = ['who']


class SkillViewSet(views.IncludeListAPI):
    """
    API endpoint that query list of skill in specify personal information
    """

    required_queries = ['who']
    translate_fields = ['tag']
    queryset = Skill.objects.order_by('id')
    serializer_class = SkillSerializer
    filter_class = SkillFilter
