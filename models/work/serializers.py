from models.experience.serializers import ExperienceSerializer

from .models import Work


class WorkSerializer(ExperienceSerializer):
    class Meta:
        model = Work
        fields = ('id', 'banner', 'company', 'position', 'website',
                  'start_date', 'end_date', 'tag', 'technical', 'summary', 'body',
                  'gallery', 'highlights', 'page_order', 'created_at', 'updated_at')


class MiniWorkSerializer(ExperienceSerializer):
    class Meta:
        model = Work
        fields = ('id', 'banner', 'company', 'position', 'website', 'summary')
