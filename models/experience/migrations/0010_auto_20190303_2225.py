# Generated by Django 2.0.8 on 2019-03-03 15:25

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('experience', '0009_auto_20190303_2222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ephoto',
            name='name',
            field=models.SlugField(default=uuid.uuid4, unique=True),
        ),
    ]
